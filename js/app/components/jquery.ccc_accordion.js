(function($) {
	$.fn.CCC_Accordion = function() {
		
		this.each(function() {
			$(this).find(".lvl_1 a.event_acc").on("click", function(e) {
				$(this).parent().children("ul.lvl_2").slideToggle();
			});

			$(this).find("li.lvl_2 a.event_acc").on("click", function(e) {
				var $lvl_2 = $(this).parent();
				var $nxt = $lvl_2.next("li.content_lvl2");
				if(!$nxt.is(":visible")){
					$nxt.slideDown();
					$lvl_2.addClass("active");
				}else{
					$nxt.slideUp();
					$lvl_2.removeClass("active");
				}
			});	

			$(this).find(".lvl_1 a.icon_info_lvl1").on("click", function(e) {
				/**
				 * Aqui va el evento de informacion de accordion nivel 1 (Titulos principales)
				 */
			});
			
			$(this).find("li.lvl_2 a.icon_info_lvl2").on("click", function(e) {
				/**
				 * Aqui va el evento de informacion de accordion nivel 2 (Titulos secundarios)
				 */
			});	
		});
	}
})(jQuery);

$(function(){
	$(".component_accordion").CCC_Accordion();
});