Namespace("com.servinf.ccc");

com.servinf.ccc.Interface = function() {
	var viewer;

	$.extend(this, {
		init: function() {
			toolComponents();
			initializeViewer();
			addEvents();
		},
		showQueryPanel: showQueryPanel,
		hideQueryPanel: hideQueryPanel
	});

	this.init();

	function initializeViewer() {
		viewer = new com.servinfo.geoviewer.GeoViewer('viewer_div', 4.5969, -74.0768, {
			imgPath: 'img/',
			appName: 'camara_de_comercio',
			mapType: google.maps.MapTypeId.ROADMAP,
			init_userMarker: true,
			requestPosition: true,
			init_drawManager: false,
			useCluster: false,
			zoom: 6,
			onZoomChangeMap: function(zoom) {

			},
			onCompleteCreateMap: function() {

			},
			onCompleteCreateGlobe: function() {

			}
		});
	}

	function toolComponents() {
		resizeViewer();
		$(window).resize(resizeViewer);
	};

	function resizeViewer() {
		var height = $(window).height();
		var new_height = height - 70;
		$("aside#sidebar").height(new_height);
		$("div#viewer_div").height(new_height);
	};

	function addEvents () {
		
		$("#goto_services").click(function(){
			console.log(isOpenQueryPanel());
			if (isOpenQueryPanel()) {
				hideQueryPanel();
			}else{
				showQueryPanel();
			};
		});
	}

	function isOpenQueryPanel () {
		return ($("aside#sidebar").hasClass("active"));
	}

	function showQueryPanel(panel) {
		$("aside#sidebar").animate({
			width: "320px"
		}, 500).addClass("active");
		$("div#viewer_div").animate({
			marginLeft: "320px"
		}, 500);
	};

	function hideQueryPanel() {
		$("aside#sidebar").animate({
			width: "0px"
		}, 500).removeClass("active");
		$("div#viewer_div").animate({
			marginLeft: "0px"
		}, 500);
	}

};

com.servinf.ccc.interface = new com.servinf.ccc.Interface();