
google.load('visualization', '1', {packages: ['corechart']});

class GoogleGraphics 
	constructor: ->

	pieChart : (node, options) ->
		optionsgrap = options.optionsgrap || {title: "Pie Chart"}
		chart = new google.visualization.PieChart node 
		data = new google.visualization.arrayToDataTable options.matrix
		if options.events isnt null
			for e,f of options.events
				console.log e,fif options.events isnt null
			for e,f of options.events
				google.visualization.events.addListener chart, e, f
				google.visualization.events.addListener chart, e, f
		chart.draw data, optionsgrap
		return 0

	lineChart: (node, options) ->
		optionsgrap = options.optionsgrap || {curveType: "function"}
		line = new google.visualization.LineChart node
		data = new  google.visualization.arrayToDataTable options.matrix
		if options.events isnt null
			for e,f of options.events
				google.visualization.events.addListener chart, e, f
		line.draw data, optionsgrap
		return 0

charts = new GoogleGraphics()