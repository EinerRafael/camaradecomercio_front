Namespace('com.servinfo.Routing');
//routing.process({name:"Polo Club", coords:'4.67245858,-74.0634278'},{name:"Escuela Militar",coords:'4.675484803,-74.068512237'});
com.servinfo.Routing = function(options){
	document.routing = this;
	var origin = {};
	var destination = {};
	var directionsService;
	var directionsDisplay;
	var response;
	var status;
	var simulacro;
	var timer;
	var currentStep;
    var objRouting;
	var geoviewer = options.geoviewer;
	var container_ok = options.container_ok;
    var controls = options.controls;
    var onOkRouting = options.onOkRouting ;
    var onErrorRouting = options.onErrorRouting;

	$.extend(this, {
		init : function(){
	 		var rendererOptions = {
            	draggable: false
	        };
	        directionsService = new google.maps.DirectionsService();
	        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
            
		},

		process : function(_origin, _destination, travelMode){
            destroy();
            objRouting =  {
                origin : "",
                destination: "",
                distance : '',
                duration : '',
                route : []        
            };

            if(_origin){
                objRouting.origin = _origin.name;
                origin = _origin.coords;
            }else{
                objRouting.origin = "Mi Posición";
                origin = geoviewer.getUserMarker().getPosition().lat()+","+geoviewer.getUserMarker().getPosition().lng();
            }
			if(_destination.coords){
                objRouting.destination = _destination.name;
                destination = _destination.coords;
             }               
            else{
                alert("La Unicacion de Destino no esta especificada");
                return;
            }                
            
			var travelM = travelMode || google.maps.DirectionsTravelMode.DRIVING; //BICYCLING , WALKING
			var request = {
	            origin: origin,
	            destination: destination,
	            travelMode: travelM,   
	            unitSystem: google.maps.DirectionsUnitSystem["METRIC"],
	            provideRouteAlternatives: true
        	}
        	directionsService.route(request, process_ServiceRoute);
		},

        getObjRouting : function(){
            return objRouting;
        },

        getObjRoutingJson: function(){
            return JSON.stringify(objRouting);
        },

        reset: function(){
            $(container_ok).hide();
            if(objRouting){
                geoviewer.deleteLayer("RUTEO");
                objRouting = null;
                $('#itinerary').html(''); 
            }
            if (timer != null) {
                    stopRoute();
                }
        },

		nextStep : function (){
        	if (currentStep < simulacro.length) {
	            geoviewer.setCenter(simulacro[currentStep].lat(), simulacro[currentStep].lng(), 75, 1000);
	            currentStep++;
	        } else 
            if (timer != null) {
	            stopRoute();
	        }
       	}        
	});

    $(controls.start).click(function(){
        run(1000);
    });
    
    $(controls.stop).click(stopRoute);

    $(controls.back).click(backStep);

    $(controls.destroy).click(destroy);

	function process_ServiceRoute (_response, _status) {
		response = _response;
		status = _status;
		if (status === google.maps.DirectionsStatus.OK) {
			$(container_ok).show();
            buildSteps();
            traceLine();
            geoviewer.setCenterBounds(response.routes[0].bounds);
            $('#itinerary').html('');
            paintItinerary(objRouting, $('#itinerary'));
        } else {
            alert(status);
        }
	}

	function traceLine(){
		simulacro = new Array();
        currentStep = 0;
        var myRoute = response.routes[0].legs[0];
        objRouting.duration = myRoute.duration.text;
        objRouting.distance = myRoute.distance.text;

        var coordArray = new Array();
        for (var i = 0; i < myRoute.steps.length; i++) {
            var path = myRoute.steps[i].path;
            for (var j = 0; j < path.length; j++) {
                if (i == 0 && j == 0) {
                	geoviewer.addMarker({
                			id: "Inicio",
							layer : "RUTEO",
							latitude : path[j].lat(),
							longitude :  path[j].lng(),
							name : objRouting.origin,
							balloon : "Inicio de La Ruta: "+objRouting.origin,
							icon: "Icon-Start.png",
							onClick: function(id, event) {
								var marker = geoviewer.getMapOverlay(id);
								var lat = marker.geometry.getPosition().lat();
								var lng = marker.geometry.getPosition().lng();
								geoviewer.showInfowindowForOverlay(id, objRouting.origin.name, event.latLng);
						}
					});
                }
                if (i == (myRoute.steps.length - 1) && j == (path.length - 1)) {
                    geoviewer.addMarker({
                    		id: "Fin",
							layer : "RUTEO",
							latitude : path[j].lat(),
							longitude :  path[j].lng(),
							name : objRouting.destination,
							balloon : "Fin de La Ruta: "+objRouting.destination,
							icon: "Icon-End.png",
							onClick: function(id, event) {
								var marker = geoviewer.getMapOverlay(id);
								var lat = marker.geometry.getPosition().lat();
								var lng = marker.geometry.getPosition().lng();
								geoviewer.showInfowindowForOverlay(id, objRouting.destination.name, event.latLng);
						}
					});
                }
                coordArray.push( path[j].lng()+ "," +path[j].lat() );
                simulacro.push(path[j]);
            }
        }

        geoviewer.addLine({
        	layer: "RUTEO",
        	panToLine: true,
        	string : coordArray.join(";"),
        	pointSeparator: ";",
        	coordSeparator : ",",
        	clickable : false
        });
	}




    function destroy(){
            $(container_ok).hide();
            if(objRouting){
                geoviewer.deleteLayer("RUTEO");
                objRouting = null;
                $('#itinerary').html(''); 
            }
            if (timer != null) {
                    stopRoute();
                }
        }   

    function stopRoute(){
        if (timer != null) {
            clearInterval(timer);
            timer = null;
        }
    }

    function backStep() {
        if (currentStep > 0) {
            currentStep--;
            geoviewer.setCenter(simulacro[currentStep].lat(), simulacro[currentStep].lng());
        }
    }

    function initStep() {
        currentStep = 0;
        geoviewer.setCenter(simulacro[currentStep].lat(), simulacro[currentStep].lng(), 75);
    }


    function buildSteps(){
        var myRoute = response.routes[0].legs[0];
        for (var i = 0; i < myRoute.steps.length; i++) {
            var instructions = myRoute.steps[i].instructions;
            var objTmp ={
                instructions: myRoute.steps[i].instructions,
                duration : myRoute.steps[i].duration.text,
                distance: myRoute.steps[i].distance.text
            };
            objRouting.route.push(objTmp);
            geoviewer.addMarker({
                            id: "Punto "+i,
                            layer : "RUTEO",
                            latitude : myRoute.steps[i].start_point.lat(),
                            longitude :  myRoute.steps[i].start_point.lng(),
                            name : "Punto "+i,
                            balloon : instructions,
                            icon: "spotlight-poi.png",
                            onClick: function(id, event) {
                                var marker = geoviewer.getMapOverlay(id);
                                var lat = marker.geometry.getPosition().lat();
                                var lng = marker.geometry.getPosition().lng();
                                geoviewer.showInfowindowForOverlay(id,  instructions, event.latLng);
                        }
                    });
        } 
    }

    function run(interval){
    	if (timer == null) {
            timer = setInterval("document.routing.nextStep()", interval);
        }
    }

	this.init();
}