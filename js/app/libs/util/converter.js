/*

  Class CoordConverter 0.1a: simpler version (JavaScript)

  Convert geographical coordinates from degrees-minutes-seconds to decimal
  notation and inverse.

  Copyright (c) 2010, Jorge Ivan Meza Martinez.
        
  http://jorgeivanmeza.com/

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA

*/

////////////////////////////////////////////////////////////////////////////////

/**
 * Class CoordConverter (simpler version)
 *
 * Convert geographical coordinates from degrees-minutes-seconds to decimal
 * notation and inverse.
 */

function CoordConverter()
{
	/**
	 * Convert coordinates from degrees-minutes-seconds to decimal notation.
	 *
	 * @Param	latitude_deg	Degrees of the latitude.
	 * @Param	latitude_min	Minutes of the latitude.
	 * @Param	latitude_sec	Seconds of the latitude.
	 * @Param	latitude_dir	"N" or "S".
	 * @Param	longitude_deg	Degrees of the longitude.
	 * @Param	longitude_min	Minutes of the longitude.
	 * @Param	longitude_sec	Seconds of the longitude.
	 * @Param	longitude_dir	"E" or "W".
	 *
	 * @Return	{"Latitude", "Longitude"}
	 *
	 * @Throws	Validation errors.
	 */

	this.GMS2Decimal = function(latitude_deg, latitude_min, latitude_sec, latitude_dir,
		                    longitude_deg, longitude_min, longitude_sec, longitude_dir)
	{
		// Initial transformations

		latitude_sign = 1;
		longitude_sign = 1;

		if(latitude_dir.toLowerCase() == "s")
			latitude_sign = -1;

		if(longitude_dir.toLowerCase() == "w")
			longitude_sign = -1;

		// Validations

		if(latitude_dir.toLowerCase() != "n" && latitude_dir.toLowerCase() != "s")
			throw "Latitude (N or S) is incorrect: " + latitude_dir;

		if(longitude_dir.toLowerCase() != "e" && longitude_dir.toLowerCase() != "w")
			throw "Longitude (E or W) is incorrect: " + longitude_dir;


		if(latitude_deg > 90 || latitude_deg < -90)
			throw "Latitude.degrees (-90 < d < 90) invalid: " + latitude_deg;

		if(latitude_min > 60 || latitude_min < 0)
			throw "Latitude.minutes (0 < m < 59) invalid: " + latitude_min;

		if(latitude_sec > 60 || latitude_sec < 0) 
			throw "Latitude.seconds (0 < s < 60) invalid: " + latitude_sec;


		if(longitude_deg > 180 || longitude_deg < -180) 
			throw "Longitude.degrees (-180 < d < 180) invalid: " + longitude_deg;

		if(longitude_min > 60 || longitude_min < 0)
			throw "Longitude.minutes (0 < m < 59) invalid: " + longitude_min;

		if(longitude_sec > 60 || longitude_sec < 0)
			throw "Longitude.seconds (0 < s < 60) invalid: " + longitude_sec;

		// Final calculation

		latitude  = (parseFloat(latitude_deg)  + (latitude_min / 60.0)  + (latitude_sec / 60.0 / 60.0))  * latitude_sign;
		longitude = (parseFloat(longitude_deg) + (longitude_min / 60.0) + (longitude_sec / 60.0 / 60.0)) * longitude_sign;

		// Packing the results

		return {
			"latitude": latitude,
                        "longitude": longitude
		};
	}

	////////////////////////////////////////////////////////////////////////

	/**
	 * Convert coordinates from decimal to degrees-minutes-seconds notation.
	 *
	 * @Param	latitude	Degrees in decimal notation.
	 * @Param	longitude	Degrees in decimal notation.
	 *
	 * @Return	{"Latitude": {"degrees", "minutes", "seconds", "dir"}, 
	 * 		 "Longitude": {"degrees", "minutes", "seconds", "dir"}}
	 *
	 * @Throws	Validation errors.
	 */

	this.Decimal2GMS = function(latitude, longitude)
	{
		// Initial transformations

		latitude_sign  = 1;
		longitude_sign = 1;

		if(latitude < 0)
			latitude_sign = -1;

		if(longitude < 0)
			longitude_sign = -1;

		// Validations

		if(latitude > 90 || latitude < -90)
			throw "Latitude.degrees (-90 < d < 90) invalid: " + latitude;

		if(longitude > 180 || longitude < -180)
			throw "Longitude.degrees (-180 < d < 180) invalid: " + longitude;

		// Final calculations

		latitude_deg = Math.floor(Math.abs(latitude));
		latitude_min = Math.floor((Math.abs(latitude) - latitude_deg) * 60);
		latitude_sec = Math.ceil(((Math.abs(latitude) - latitude_deg) * 60 - latitude_min) * 60);
		latitude_dir  = (latitude_sign > 0) ? "N" : "S";

		longitude_deg = Math.floor(Math.abs(longitude));
		longitude_min = Math.floor((Math.abs(longitude) - longitude_deg) * 60);
		longitude_sec = Math.ceil(((Math.abs(longitude) - longitude_deg) * 60 - longitude_min) * 60);
		longitude_dir  = (longitude_sign > 0) ? "E" : "W";

		// Packing the results

		return {
			"latitude": {
				"degrees": Math.abs(latitude_deg),
				"minutes": latitude_min,
				"seconds": Math.round(latitude_sec),
				"dir": latitude_dir
			},
                        "longitude": {
				"degrees": Math.abs(longitude_deg),
				"minutes": longitude_min,
				"seconds": Math.round(longitude_sec),
				"dir": longitude_dir
			}
		};
	}

	////////////////////////////////////////////////////////////////////////

} // End of class CoordConverter

////////////////////////////////////////////////////////////////////////////////

