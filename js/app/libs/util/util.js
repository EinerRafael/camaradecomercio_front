/*
 * Document: util.js
 * Author: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 * 
 * Funciones utiles.
 */


String.prototype.format = function() {
	var literal = this;
	for (var i = 0; i < arguments.length; i++) {
		var regex = new RegExp('\\{' + i + '\\}', 'g');
		literal = literal.replace(regex, arguments[i]);
	}
	return literal;
};

String.prototype.trim = function(){return this.replace(/^\s+|\s+$/g, '');};

String.prototype.ltrim = function(){return this.replace(/^\s+/,'');};

String.prototype.rtrim = function(){return this.replace(/\s+$/,'');};

String.prototype.fulltrim = function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};

String.prototype.reverse = function() {
	var i = this.length, s = "";
	while (--i >= 0)
		s += this.charAt(i);
	return s;
};



function addCommas(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0) from += len;

        for (; from < len; from++) {
            if (from in this && this[from] === elt) return from;
        }
        return -1;
    };
}

google.maps.Polygon.prototype.getBounds = function() {
	var bounds = new google.maps.LatLngBounds();
	var paths = this.getPaths();
	var path;
	for (var i = 0; i < paths.getLength(); i++) {
		path = paths.getAt(i);
		for (var j = 0; j < path.getLength(); j++) {
			bounds.extend(path.getAt(j));
		}
	}
	return bounds;
};



google.maps.Polyline.prototype.getBounds = function() {
	var bounds = new google.maps.LatLngBounds();
	var path = this.getPath();
	path.forEach(function(element, index){
		bounds.extend(element);
	});
	return bounds;
};

function dispatchCustomEvent(name, params) {
	obj = {
		detail: {}
	};
	params || (params = obj);
	$.event.trigger({
		type: name,
		detail: params.detail
	});
};


