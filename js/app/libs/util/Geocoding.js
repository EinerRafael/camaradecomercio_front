
Namespace('com.servinfo.Geocoding');

com.servinfo.Geocoding = function(){
	var geocoder;

	$.extend(this, {
		init: function(){
			geocoder = new google.maps.Geocoder();
		},

		geocode : function(address, callback){
			geocoder.geocode( { 'address': address}, callback);
		},

		inverse : function(lat, lng, callback){
			var latlng = new google.maps.LatLng(lat, lng);
			geocoder.geocode({'latLng': latlng}, callback);
		}
	});	

	this.init();
}