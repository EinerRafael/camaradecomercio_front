/*
 * Document: consumer_all.js
 * Author: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 * 
 * Clase para consumir webservices rest.
 */

//Espacio de nombres
Namespace('com.servinfo.consumer');



//Clase Consumer
com.servinfo.consumer.Consumer = function(options){
	
	var urls = new com.servinfo.Hash();
	// var consumerBaseUrl = 'http://127.0.0.1:8080/geoperudal/api';
	var consumerBaseUrl = 'http://54.235.135.60:8040/pec/api';
	urls.setItem('entidad_radio', consumerBaseUrl + '/entity/latitude/{0}/longitude/{1}/radius/{2}');
	urls.setItem('entidad_tag', consumerBaseUrl + '/entity/latitude/{0}/longitude/{1}/radius/{2}/tags/{3}');
	urls.setItem('sitio_cat', consumerBaseUrl + '/place/latitude/{0}/longitude/{1}/radius/{2}/cat/{3}');
	urls.setItem('events', consumerBaseUrl + '/event/'); 



	$.extend(this, {
		//Función de inicialización
		init: function(){
		},
		
		
		
		getEntity: function(options){
			var url = urls.getItem(options.entity);
			if(url === undefined){
				alert('La entidad soliscitada no es correcta.');
				return false;
			}
			// showLoading();
			$.ajax(url.format(options.latitude, options.longitude, options.radius, options.term), {
				contentType: 'application/json',
				cache: 'false',
				type: 'GET',
				data: undefined,//JSON.stringify(params),
				success: function(data, textStatus, jqXHR){
					if(data.success){
						var list = [];
						for(var i in data.object){
							var item = data.object[i];
							switch(options.entity){
								case 'entidad_radio':
								case 'entidad_tag':{
									list.push({
										id: options.entity + item.id,
										title: item.name,
										info: {
											'Nombre': item.name,
											'Iniciales': item.initials,
											'Dirección': item.address,
											'Teléfono': item.phone,
											'Fax': item.fax,
											'Sitio Web': item.website,
											'Email': item.email,
											'Horario': item.attention
										},
										latitude: item.latitude,
										longitude: item.longitude
									});
									break;
								}
								case 'sitio_cat':{
									var icon = '';
									switch(options.term){
										case 'RESTAURANTES': icon = 'restaurant.png'; break;
										case 'HOSPITALES': icon = 'hospital.png'; break;
										case 'HOTELES': icon = 'hotel.png'; break;
										case 'CAI': icon = 'police.png'; break;
									}
									list.push({
										id: options.term + i,
										title: item.name,
										icon: icon,
										info: {
											'Nombre': item.name,
											'Dirección': item.address,
											'Teléfono': item.phone,
											'Barrio': item.neighborhood,
											'Localidad': item.locality
										},
										latitude: item.latitude,
										longitude: item.longitude
									});
									break;
								}
								case 'events':{
									list.push({
										id: options.term + i,
										title: item.name,
										icon: 'event.png',
										info: {
											'Nombre': item.name,
											'Dirección': item.address,
											'Fecha': item.date,
											'Descripción': item.description
										},
										latitude: item.latitude,
										longitude: item.longitude
									});
									break;
								}
							}
						}
						if(options.callback !== undefined){
							options.callback(list, options.layer);
						}
					}else{
						alert('No se pudieron cargar los puntos. ' + data.message);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert('Ocurrió un error al intentar cargar los puntos.');
				},
				complete: function(){
					// hideLoading();
				}
			});
		}
	});
	
	this.init();	

};



var consumer;//Instancia del consumidor
consumer = new com.servinfo.consumer.Consumer();




