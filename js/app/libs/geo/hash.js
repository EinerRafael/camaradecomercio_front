/*
 * Document: hash.js
 * Author: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 * 
 * Un Hash Map.
 */


Namespace('com.servinfo');



com.servinfo.Hash = function(){
	

	var length = 0;
	var items = new Array();


	$.extend(this, {
		
		init: function(){
			for (var i = 0; i < arguments.length; i += 2) {
				if (arguments[i + 1] !== undefined) {
					items[arguments[i]] = arguments[i + 1];
					length++;
				}
			}
		},
				
				
				
		setItem: function(in_key, in_value){
			var tmp_previous;
			if (in_value !== undefined) {
				if (items[in_key] === undefined) {
					length++;
				} else {
					tmp_previous = items[in_key];
				}
				items[in_key] = in_value;
			}
			return tmp_previous;
		},
				
				
				
		getItem: function(in_key) {
			return items[in_key];
		},



		getLastItem: function(){
			return items[items.length - 1];
		},
				
				
				
		hasItem: function(in_key){
			return items[in_key] !== undefined;
		},
				
				
				
		removeItem: function(in_key){
			var tmp_previous;
			if (items[in_key] !== undefined) {
				length--;
				var tmp_previous = items[in_key];
				delete items[in_key];
			}
			return tmp_previous;
		},
				
				
				
		clear: function() {
			for (var i in items) {
				delete items[i];
			}
			length = 0;
		},
				
				
				
		keys: function() {
			var tmpArray = new Array();
			var j = 0;
			for (var i in items) {
				tmpArray[j++] = i;
			}
			return tmpArray;
		},
				
				
				
		each: function(callback){
			for (var i in items) {
				callback(items[i], i);
			}
		}
	});
	this.init();
};