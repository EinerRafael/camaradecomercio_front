1456

/*
 * Document: geoviewer.js
 * Author: Ing. Edwin Fuentes Amin - efuentesamin@gmail.com - 3013427595
 *
 * Clase principal del geo visor.
 */

//Espacio de nombres
Namespace('com.servinfo.geoviewer');

//Constantes tipo de mapa
com.servinfo.geoviewer.ViewerType = {};
com.servinfo.geoviewer.ViewerType.MAP = 0;
com.servinfo.geoviewer.ViewerType.EARTH = 1;

//Constantes Creacion Instancias
com.servinfo.geoviewer.Instance_MAP = 0;
com.servinfo.geoviewer.Instance_EARTH = 1;

//Constantes dibujo
com.servinfo.geoviewer.DrawMode = {};
com.servinfo.geoviewer.DrawMode.PAN = 0;
com.servinfo.geoviewer.DrawMode.MARKER = 1;
com.servinfo.geoviewer.DrawMode.LINE = 2;
com.servinfo.geoviewer.DrawMode.POLY = 3;
com.servinfo.geoviewer.DrawMode.CIRCLE = 4;
com.servinfo.geoviewer.DrawMode.RECTANGLE = 5;
com.servinfo.geoviewer.DrawMode.ERASE1 = 6;
com.servinfo.geoviewer.DrawMode.ERASE2 = 7;

//Constantes ruteo
com.servinfo.geoviewer.routing = {};
com.servinfo.geoviewer.routing.WALK_ROUTING = 0;
com.servinfo.geoviewer.routing.CAR_ROUTING = 1;


google.load("earth", "1");
//Clase GeoViewer

var kmlClass = function() {
	this.url = "";
	this.isVisible = false;
	this.objMap = null;
	this.objEarth = null;
}

com.servinfo.geoviewer.GeoViewer = function(container, lat, lon, options) {

	var mapContainerId = container || 'map-div'; //Id del contenedor del visor
	var latitude = lat || 150.644; //Latitud para el centro del mapa
	var longitude = lon || -34.397; //Longitud para el centro del mapa
	var zoom = options.zoom || 13; //Nievel de acercamiento
	if (options !== undefined) {
		var imgPath = options.imgPath || 'img/';
	}
	var viewerType = options.viewerType || com.servinfo.geoviewer.ViewerType.MAP; //Tipo de visor
	var appName = options.appName || "geoviewer";
	var mapType = options.mapType || google.maps.MapTypeId.ROADMAP; //google.maps.MapTypeId.SATELLITE;
	var useCluster = options.useCluster || false;
	var mapObject = undefined; //Mapa
	var earthObject = undefined; //Globo
	var earthViewChangedTimer; //Timer auxiliar para evento de cambio de vista en el globo
	var dorequestPosition = options.requestPosition || false;
	var earthViewChangedTimer; //Timer auxiliar para evento de cambio de vista en el globo
	var drawingManager;
	var mapLayers = new Object(); //Capas en mapa
	var earthLayers = new Object(); //Capas en globo
	var currentLayer = 0; //Id de la capa actual
	var trafficLayer; //Capa de tráfico.
	var weatherLayer; //Capa de clima.
	var userMarker; //Posición del usuario.
	var clusterer; //Clusterer para los marcadores
	var panoramio;
	var gex;

	var drawMode = com.servinfo.geoviewer.DrawMode.PAN; //Modo de dibujo
	var drawing = false; //Está dibujando?
	var marker = undefined;
	var polyLine = undefined;
	var polygon = undefined;
	var colors = ['0080af', '0cc6b2', '44d909', 'b86800', 'd70000'];
	var infowindow;
	var kmls = new com.servinfo.Hash();

	var centering = false;
	var batchIds;
	var batchTotal;
	var batchCount;
	var batchCallback;

	var geocoder;
	var directionsService;
	var directionsRenderer;
	var currentCity;
	var currentCountry;
	var init_userMarker = options.init_userMarker || false;
	var init_drawManager = options.init_drawManager || false;


	//Auxiliares
	var onCompleteCreateGlobe = options.onCompleteCreateGlobe || undefined;
	var instances = options.instances || []; //com.servinfo.geoviewer.Instance_MAP
	var onZoomChangeMap = options.onZoomChangeMap || undefined;
	var onCompleteCreateMap = options.onCompleteCreateMap || undefined;


	$.extend(this, {
		//Función de inicialización
		init: function() {
			if (viewerType === com.servinfo.geoviewer.ViewerType.MAP) {
				zoom = 13;
			} else {
				zoom = 900;
			}

			createMap(); //Creación del mapa
			//createGlobe(); //Creación del globo	

			infowindow = new google.maps.InfoWindow();
			initializeDraw();
		},

		getViewType: function() {
			return viewerType;
		},

		requestPosition: function(dozoom) {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(location) {
					successPosition(location, dozoom);
				}, failurePosition);
			}
		},


		addKml: function(id, url, map, noAbstracview) {
			kmls.removeItem(id);
			this.removeKml(id);
			kml = new kmlClass();
			kml.url = url;
			kmls.setItem(id, kml);
			this.onOffKml(id, map, noAbstracview);
		},

		getKml: function(id) {
			return kmls.getItem(id);
		},

		onOffKml: function(id, map, noAbstracview) {
			var kmlO = kmls.getItem(id);
			if (kmlO) {
				if (!kmlO.isVisible) {
					var kmlObjectM = new google.maps.KmlLayer(kmlO.url);
					if (map === undefined) {
						kmlObjectM.setMap(mapObject);
						kmlO.objMap = kmlObjectM;
						google.earth.fetchKml(earthObject, kmlO.url, function(kmlObjectE) {
							if (kmlObjectE) {
								earthObject.getFeatures().appendChild(kmlObjectE);
								kmlO.objEarth = kmlObjectE;
							}
						});
					}

					if (map === 'maps') {
						kmlObjectM.setMap(mapObject);
						kmlO.objMap = kmlObjectM;
					}

					if (map === 'earth') {
						google.earth.fetchKml(earthObject, kmlO.url, function(kmlObjectE) {
							if (kmlObjectE) {
								earthObject.getFeatures().appendChild(kmlObjectE);
								kmlO.objEarth = kmlObjectE;
								if (kmlObjectE.getAbstractView() != null) {
									earthObject.getView().setAbstractView(kmlObjectE.getAbstractView());
								} else {
									if (noAbstracview)
										noAbstracview();
								}

							}
						});
					}

					kmlO.isVisible = true;
				} else {
					if (kmlO.objMap)
						kmlO.objMap.setMap(null);

					if (kmlO.objEarth)
						earthObject.getFeatures().removeChild(kmlO.objEarth);

					kmlO.isVisible = false;
				}
			}

		},

		removeKml: function(id) {
			var kmlO = kmls.getItem(id);
			if (kmlO) {
				if (kmlO.isVisible) {
					if (kmlO.objMap)
						kmlO.objMap.setMap(null);

					if (kmlO.objEarth)
						earthObject.getFeatures().removeChild(kmlO.objEarth);
				}
				kmls.removeItem(id);
			}

		},

		removeAllKml: function() {
			var keys = kmls.keys();
			for (var i = 0; i < keys.length; i++) {
				this.removeKml(keys[i]);
			}
		},

		setZoom: function(z) {
			mapObject.setZoom(z);
			if (earthObject !== undefined && viewerType === com.servinfo.geoviewer.ViewerType.EARTH) {
				var earthZoom = zoomMapToEarth(z);
			}
		},

		reset: function() {
			var zoom = mapObject.getZoom();
			var center = mapObject.getCenter();
			google.maps.event.trigger(mapObject, 'resize');
			mapObject.setZoom(zoom);
			mapObject.setCenter(center);
		},

		switchViewerType: function(type) {
			if (viewerType === type) {
				return;
			}
			viewerType = type;
			infowindow.close();
			switch (type) {
				case com.servinfo.geoviewer.ViewerType.EARTH:
					{
						zoom = zoomMapToEarth(mapObject.getZoom());
						/*$('#mapDiv').css('z-index', '-1');
					$('#globeDiv').css('z-index', '0');*/
						$('#mapDiv').css('visibility', 'hidden');
						$('#globeDiv').css('visibility', 'visible');
						/*$('#mapDiv').hide();
					$('#globeDiv').show();*/
						this.setCenter(latitude, longitude);
						break;
					}
				case com.servinfo.geoviewer.ViewerType.MAP:
					{
						if (earthObject !== undefined) {
							/*$('#mapDiv').css('z-index', '0');
						$('#globeDiv').css('z-index', '-1');*/
							$('#mapDiv').css('visibility', 'visible');
							$('#globeDiv').css('visibility', 'hidden');
							viewerType = com.servinfo.geoviewer.ViewerType.MAP;
							var lookAt = earthObject.getView().copyAsLookAt(earthObject.ALTITUDE_RELATIVE_TO_GROUND);
							zoom = zoomEarthToMap(lookAt.getRange());
							/*$('#mapDiv').show();
						$('#globeDiv').hide();*/
							this.setCenter(latitude, longitude);
						}
						break;
					}
			}
		},



		setDrawMode: function(mode) {
			finishDraw();
			mode = parseInt(mode);
			if (drawMode === mode) {
				return;
			}
			drawMode = mode;
			switch (mode) {
				case com.servinfo.geoviewer.DrawMode.MARKER:
				case com.servinfo.geoviewer.DrawMode.LINE:
				case com.servinfo.geoviewer.DrawMode.POLY:
				case com.servinfo.geoviewer.DrawMode.CIRCLE:
				case com.servinfo.geoviewer.DrawMode.RECTANGLE:
				case com.servinfo.geoviewer.DrawMode.ERASE1:
					{
						mapObject.setOptions({
							draggable: false,
							draggableCursor: 'move'
						});
						break;
					}
				case com.servinfo.geoviewer.DrawMode.PAN:
				default:
					{
						drawMode = com.servinfo.geoviewer.DrawMode.PAN;
						mapObject.setOptions({
							draggable: true,
							draggableCursor: '',
							draggingCursor: ''
						});
						break;
					}
			}
		},



		getDrawMode: function() {
			return drawMode;
		},



		getMap: function() {
			return mapObject;
		},

		getEarth: function() {
			return earthObject;
		},


		setCenter: function(lat, lon, degrees, range, headling) {
			latitude = parseFloat(lat);
			longitude = parseFloat(lon);
			if (viewerType === com.servinfo.geoviewer.ViewerType.MAP) {
				mapObject.panTo(new google.maps.LatLng(latitude, longitude));
				mapObject.setZoom(zoom);
			}
			if (viewerType === com.servinfo.geoviewer.ViewerType.EARTH && earthObject !== undefined) {
				if (earthObject !== undefined) {
					var lookAt = earthObject.getView().copyAsLookAt(earthObject.ALTITUDE_RELATIVE_TO_GROUND);
					lookAt.setLatitude(latitude);
					lookAt.setLongitude(longitude);
					lookAt.setRange(zoom);
					if (degrees)
						lookAt.setTilt(degrees);
					if (range)
						lookAt.setRange(range);
					if (headling)
						lookAt.setHeading(headling);

					earthObject.getView().setAbstractView(lookAt);
				}
			}
		},

		setCenterBounds: function(bounds) {
			var boundstmp = new geo.Bounds();
			boundstmp.extend(new geo.Point(bounds.getNorthEast().lat(), bounds.getNorthEast().lng()));
			boundstmp.extend(new geo.Point(bounds.getSouthWest().lat(), bounds.getSouthWest().lng()));
			var view = gex.view.createBoundsView(boundstmp, {
				aspectRatio: 1.0
			});
			earthObject.getView().setAbstractView(view);
		},


		zoomIn: function() {
			mapObject.setZoom(mapObject.getZoom() + 1);
		},



		zoomOut: function() {
			mapObject.setZoom(mapObject.getZoom() - 1);
		},

		getUserMarker: function() {
			return userMarker;
		},



		//		getCenter: function(){
		//			if(viewerType === com.servinfo.geoviewer.ViewerType.MAP){
		//				return mapObject.getCenter();
		//			}
		//			if(viewerType === com.servinfo.geoviewer.ViewerType.EARTH){
		//				return new google.maps.LatLng(this.getLatitude(), this.getLongitude());
		//			}
		//		},



		//Función para retornar la latitud del centro del mapa
		//		getLatitude: function(){
		//			return latitude;
		//		},



		//Función para retornar la longitud del centro del mapa
		//		getLongitude: function(){
		//			return longitude;
		//		},



		beginBatch: function(total, callback) {
			centering = true;
			batchIds = new Array();
			batchTotal = total;
			batchCount = 0;
			batchCallback = callback;
		},



		decrementBatch: function() {
			batchTotal--;
			if (batchTotal === 0) {
				centerBatch();
			}
		},



		fitBounds: function(bounds) {
			mapObject.fitBounds(bounds);
			if (earthObject) {
				this.setCenterBounds(bounds);
			}
			//			if(viewerType === com.servinfo.geoviewer.ViewerType.MAP){
			//				mapObject.fitBounds(bounds);
			//			}
			//			if(viewerType === com.servinfo.geoviewer.ViewerType.EARTH){
			//				console.log(bounds.getNorthEast().lat());
			//				console.log(bounds.getSouthWest().lat());
			//				console.log(bounds.getNorthEast().lng());
			//				console.log(bounds.getSouthWest().lng());
			//				earthObject.getView().getViewportGlobeBounds().setBox(bounds.getNorthEast().lat(), bounds.getSouthWest().lat(), bounds.getNorthEast().lng(), bounds.getSouthWest().lng(), 0);
			//			}
		},



		addMarker: function(options) {
			return addMarker(options);
		},



		addLine: function(options) {
			infowindow.close();
			options = options || {};
			var id = options.id || '' + new Date().getTime();
			id = 'Line_' + id;
			if (this.getMapOverlay(id) !== undefined) {
				this.showOverlay(id);
				return;
			}
			var name = options.name || id;
			var layer = options.layer || currentLayer;

			var array = options.array;
			var latitudeField = options.latitudeField || 'latitude';
			var longitudeField = options.longitudeField || 'longitude';

			var string = options.string;
			var pointSeparator = options.pointSeparator || ',';
			var coordSeparator = options.coordSeparator || ' ';

			var panToLine = options.panToLine;

			var color = options.color || colors[Math.floor(Math.random() * (colors.length))];
			color = color.replace('#', '');
			var clickable = options.clickable || true;
			var onClick = options.onClick;
			var onComplete = options.onComplete;

			checkLayer(layer);

			var coordinates = new Array(); //Para mapa
			if (earthObject !== undefined) {
				var outer = earthObject.createLineString(''); //Para el globo
				outer.setAltitudeMode(earthObject.ALTITUDE_CLAMP_TO_GROUND);
			}
			if (array !== undefined) {
				for (var i in array) {
					var point = array[i];
					coordinates.push(new google.maps.LatLng(point[latitudeField], point[longitudeField]));
					if (earthObject !== undefined) {
						outer.getCoordinates().pushLatLngAlt(point[latitudeField], point[longitudeField], 0);
					}
				}
			} else if (string !== undefined) {
				var points = string.trim().split(pointSeparator);
				for (var i in points) {
					var coords = points[i].trim().split(coordSeparator);
					coordinates.push(new google.maps.LatLng(coords[1], coords[0]));
					if (earthObject !== undefined) {
						outer.getCoordinates().pushLatLngAlt(parseFloat(coords[1]), parseFloat(coords[0]), 0);
					}
				}
			}
			var line = new google.maps.Polyline({
				path: coordinates,
				strokeColor: "#" + color,
				strokeOpacity: 1,
				strokeWeight: options.strokeWeight || 2,
				clickable: clickable
			});
			line.setMap(mapObject);
			google.maps.event.addListener(line, 'click', function(event) {
				if (onClick !== undefined) {
					onClick(id, event);
				}
			});
			if (onComplete !== undefined) {
				onComplete(poly);
			}
			var obj = {
				type: 'line',
				id: id,
				name: name,
				visible: true,
				color: '#' + color,
				geometry: line,
				info: options.info
			};
			mapLayers[layer].overlays.setItem(id, obj);
			if (panToLine === true) {
				var bounds = line.getBounds();
				mapObject.fitBounds(bounds);
				mapObject.setCenter(bounds.getCenter());
			}
			if (centering === true) {
				batchIds.push(id);
				batchCount++;
				if (batchCount === batchTotal) {
					centerBatch();
				}
			}

			if (earthObject !== undefined) {
				var placemark = earthObject.createPlacemark('');
				outer.setAltitudeMode(earthObject.ALTITUDE_CLAMP_TO_GROUND);
				placemark.setGeometry(outer);
				placemark.setStyleSelector(earthObject.createStyle(''));
				var lineStyle = placemark.getStyleSelector().getLineStyle();
				lineStyle.setWidth(5);
				lineStyle.getColor().set('FF' + color.reverse());
				earthObject.getFeatures().appendChild(placemark);
				var obj = {
					type: 'line',
					id: id,
					name: id,
					visible: true,
					color: '#' + color.reverse(),
					geometry: placemark
				};
				earthLayers[layer].overlays.setItem(id, obj);
			}

			return id;
		},



		addPolygon: function(options) {
			infowindow.close();
			options = options || {};
			var id = options.id || '' + new Date().getTime();
			id = 'Poly_' + id;
			if (this.getMapOverlay(id) !== undefined) {
				this.showOverlay(id);
				return;
			}
			var category = options.category || 0;
			var name = options.name || id;
			var layer = options.layer || currentLayer;

			var array = options.array;
			var latitudeField = options.latitudeField || 'latitude';
			var longitudeField = options.longitudeField || 'longitude';

			var string = options.string;
			var pointSeparator = options.pointSeparator || ',';
			var coordSeparator = options.coordSeparator || ' ';

			var panToPoly = options.panToPoly;

			var color = options.color || colors[0]; //colors[Math.floor(Math.random() * (colors.length))];
			color = color.replace('#', '');
			var clickable = options.clickable || true;
			var onClick = options.onClick;
			var onComplete = options.onComplete;
			var balloon = options.balloon;
			var outer = undefined;
			checkLayer(layer);

			var coordinates = new Array(); //Para mapa
			if (earthObject !== undefined) {
				outer = earthObject.createLinearRing(''); //Para el globo				
				outer.setAltitudeMode(earthObject.ALTITUDE_CLAMP_TO_GROUND);
			}
			if (array !== undefined) {
				for (var i in array) {
					var point = array[i];
					coordinates.push(new google.maps.LatLng(point[latitudeField], point[longitudeField]));
					if (earthObject !== undefined) {
						outer.getCoordinates().pushLatLngAlt(point[latitudeField], point[longitudeField], 0);
					}
				}
			} else if (string !== undefined) {
				var points = string.trim().split(pointSeparator);
				for (var i in points) {
					var coords = points[i].trim().split(coordSeparator);
					coordinates.push(new google.maps.LatLng(coords[1], coords[0]));
					if (earthObject !== undefined) {
						outer.getCoordinates().pushLatLngAlt(parseFloat(coords[1]), parseFloat(coords[0]), 0);
					}
				}
			}
			var poly = new google.maps.Polygon({
				paths: coordinates,
				strokeColor: options.strokeColor || "#" + color,
				strokeOpacity: 1,
				strokeWeight: 2,
				fillColor: "#" + color,
				fillOpacity: options.fillOpacity || 0.3,
				clickable: clickable
			});
			poly.setMap(mapObject);
			google.maps.event.addListener(poly, 'click', function(event) {
				if (onClick !== undefined) {
					onClick(id, event);
				}
			});
			if (onComplete !== undefined) {
				onComplete(poly);
			}
			var obj = {
				type: 'poly',
				id: id,
				category: category,
				name: name,
				visible: true,
				color: '#' + color,
				geometry: poly,
				info: options.info
			};
			mapLayers[layer].overlays.setItem(id, obj);
			if (panToPoly === true) {
				var bounds = poly.getBounds();
				mapObject.fitBounds(bounds);
				mapObject.setCenter(bounds.getCenter());
			}
			if (centering === true) {
				batchIds.push(id);
				batchCount++;
				if (batchCount === batchTotal) {
					centerBatch();
				}
			}

			if (earthObject !== undefined) {
				var placemark = earthObject.createPlacemark('');
				poly = earthObject.createPolygon('');
				poly.setAltitudeMode(earthObject.ALTITUDE_CLAMP_TO_GROUND);
				poly.setOuterBoundary(outer);
				placemark.setGeometry(poly);
				placemark.setStyleSelector(earthObject.createStyle(''));
				var lineStyle = placemark.getStyleSelector().getLineStyle();
				lineStyle.setWidth(3);
				lineStyle.getColor().set('FF' + color.reverse());
				var polyStyle = placemark.getStyleSelector().getPolyStyle();
				polyStyle.getColor().set('88' + color.reverse());

				google.earth.addEventListener(placemark, 'click', function(event) {
					event.preventDefault();
					var balloonObj = earthObject.createHtmlStringBalloon('');
					var placeAux = earthObject.createPlacemark('');
					var point = earthObject.createPoint('');
					point.setLatitude(parseFloat(event.getLatitude()));
					point.setLongitude(parseFloat(event.getLongitude()));
					placeAux.setGeometry(point);
					balloonObj.setFeature(placeAux);
					//balloonObj.setMaxWidth(300);
					balloonObj.setContentString(balloon);
					earthObject.setBalloon(balloonObj);
				});
				earthObject.getFeatures().appendChild(placemark);
				var obj = {
					type: 'poly',
					id: id,
					name: name,
					visible: true,
					color: '#' + color.reverse(),
					geometry: placemark
				};
				earthLayers[layer].overlays.setItem(id, obj);
			}

			return id;
		},



		showInfowindowForOverlay: function(id, content, position, marker) {
			infowindow.setContent(content);
			infowindow.setPosition(position);
			if (marker) {
				infowindow.open(mapObject, marker.geometry);
			} else {
				infowindow.open(mapObject);
			}

		},



		getMapOverlay: function(id) {
			return getMapOverlay(id);
		},



		getEarthOverlay: function(id) {
			return getEarthOverlay(id);
		},



		showOverlay: function(id) {
			showOverlay(id);
		},



		hideOverlay: function(id) {
			var overlay = getMapOverlay(id);
			if (overlay !== undefined) {
				overlay.geometry.setOptions({
					map: null
				});
				if (overlay.type === 'marker') {
					clusterer.removeMarker(overlay.geometry);
					overlay.geometry.setMap(null);
				}
			}
			clusterer.redraw();
			overlay = getEarthOverlay(id);
			if (overlay !== undefined) {
				earthObject.getFeatures().removeChild(overlay.geometry);
			}
		},



		deleteOverlay: function(id) {
			for (var layer in mapLayers) {
				mapLayers[layer].overlays.each(function(overlay) {
					if (overlay.id === id) {
						overlay.geometry.setOptions({
							map: null
						});
						mapLayers[layer].overlays.removeItem(id);
						clusterer.removeMarker(overlay.geometry);
					}
				});
			}
			clusterer.redraw();
			for (var layer in earthLayers) {
				earthLayers[layer].overlays.each(function(overlay) {
					if (overlay.id === id) {
						earthObject.getFeatures().removeChild(overlay.geometry);
						earthLayers[layer].overlays.removeItem(id);
					}
				});
			}
		},



		newLayer: function(layer) {
			if (layer === undefined || layer === '' || layer < 0) {
				layer = Object.keys(mapLayers).length;
			}
			checkLayer(layer);
			this.setCurrentLayer(layer);
		},



		setCurrentLayer: function(layer) {
			if (mapLayers[layer] !== undefined) {
				currentLayer = layer;
			}
		},



		renameLayer: function(id, newName) {
			renameLayer(id, newName);
		},



		setLayerId: function(oldId, newId) {
			mapLayers[newId] = mapLayers[oldId];
			delete(mapLayers[oldId]);
			earthLayers[newId] = earthLayers[oldId];
			delete(earthLayers[oldId]);
		},



		getLayers: function() {
			return mapLayers;
		},



		//Función para ocultar una capa (o todas)
		hideLayer: function(layer) {
			if (layer === undefined || layer === '') {
				for (var i in mapLayers) {
					hideLayer(i);
				}
			} else {
				hideLayer(layer);
			}
		},



		//Función para mostrar una capa (o todas)
		showLayer: function(layer) {
			if (layer === undefined || layer === '') {
				for (var i in mapLayers) {
					showLayer(i);
				}
			} else {
				showLayer(layer);
			}
		},



		//Función para eliminar una capa (o todas)
		deleteLayer: function(layer) {
			if (layer === undefined || layer === '') {
				for (var i in mapLayers) {
					deleteLayer(i);
				}
			} else {
				deleteLayer(layer);
			}
		},



		//Ruteo.
		routing: function(options) {
			infowindow.close();
			var destination = options.destination || [];
			var origin = options.origin || undefined;
			var type = options.type || com.servinfo.geoviewer.routing.WALK_ROUTING;
			var okRouting = options.okRouting || undefined;
			if (type === com.servinfo.geoviewer.routing.WALK_ROUTING) {
				type = google.maps.TravelMode.WALKING;
			} else {
				type = google.maps.TravelMode.DRIVING;
			}
			var itineraryContainer = options.itineraryContainer || undefined;

			if (itineraryContainer !== undefined && itineraryContainer !== '') {
				directionsRenderer.setPanel($('#' + itineraryContainer)[0]);
			}

			if (destination.length < 2) {
				alert('Coordenadas incorrectas.');
				return false;
			}
			if (origin !== undefined && origin.length < 2) {
				alert('Coordenadas incorrectas.');
				return false;
			}
			var orig;
			if (origin === undefined) {
				orig = new google.maps.LatLng(userMarker.getPosition().lat(), userMarker.getPosition().lng());
			} else {
				orig = new google.maps.LatLng(origin[0], origin[1]);
			}
			var dest = new google.maps.LatLng(destination[0], destination[1]);

			var request = {
				origin: orig,
				destination: dest,
				travelMode: type
			};
			directionsService.route(request, function(result, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsRenderer.setMap(mapObject);
					directionsRenderer.setDirections(result);

					if (okRouting !== undefined) {
						okRouting();
					}
				} else {
					alert("No Se Encontro Ruta Hasta Este Punto");
				}
			});
		},



		deleteRoute: function() {
			directionsRenderer.setMap(null);
		},



		//StreetView.
		openStreetView: function(options) {
			infowindow.close();
			var coordinates = options.coordinates || [];

			if (coordinates.length < 2) {
				alert('Coordenadas incorrectas.');
				return false;
			}
			var noStreetViewZone = options.noStreetViewZone || function() {
					alert('No Se Encontro Vista Para esta Zona')
				};

			mapObject.getStreetView().setPosition(new google.maps.LatLng(coordinates[0], coordinates[1]));
			var toggle = mapObject.getStreetView().getVisible();
			if (toggle == false)
				mapObject.getStreetView().setVisible(true);
			else
				noStreetViewZone();
		},



		//Ruteo.
		routing2: function(options) {
			var coordinates = options.coordinates || [];
			var type = options.type || com.servinfo.geoviewer.routing.WALK_ROUTING;
			if (type === com.servinfo.geoviewer.routing.WALK_ROUTING) {
				type = google.maps.TravelMode.WALKING;
			} else {
				type = google.maps.TravelMode.DRIVING;
			}
			var itineraryContainer = options.itineraryContainer || undefined;

			if (itineraryContainer !== undefined && itineraryContainer !== '') {
				directionsRenderer.setPanel($('#' + itineraryContainer)[0]);
			}

			if (coordinates.length < 2) {
				alert('Debe especificar al menos dos direcciones.');
				return false;
			}
			var origin = coordinates[0] + ' ' + currentCity + ', ' + currentCountry;
			var destination = coordinates[coordinates.length - 1] + ' ' + currentCity + ', ' + currentCountry;

			if (origin === undefined || origin === '' || destination === undefined || destination === '') {
				alert('El origen y/o  el destino son incorrectos.');
				return false;
			}

			coordinates.splice(0, 1);
			coordinates.splice(coordinates.length - 1, 1);

			var request = {
				origin: origin,
				destination: destination,
				waypoints: coordinates,
				travelMode: type
			};
			directionsService.route(request, function(result, status) {
				console.log(result);
				if (status === google.maps.DirectionsStatus.OK) {
					directionsRenderer.setDirections(result);
				}
			});
		},

		enableTraffic: function() {
			trafficLayer.setMap(mapObject);
		},

		disableTraffic: function() {
			trafficLayer.setMap(null);
		},

		enablePanoramio: function() {
			panoramio.setMap(mapObject);
		},

		disablePanoramio: function() {
			panoramio.setMap(null);
		},

		switchPanoramio: function() {
			if (panoramio.getMap() === undefined || panoramio.getMap() === null) {
				this.enablePanoramio();
			} else {
				this.disablePanoramio();
			}
		},


		switchTraffic: function() {
			if (trafficLayer.getMap() === undefined || trafficLayer.getMap() === null) {
				this.enableTraffic();
			} else {
				this.disableTraffic();
			}
		},



		enableWeather: function() {
			weatherLayer.setMap(mapObject);
		},



		disableWeather: function() {
			weatherLayer.setMap(null);
		},



		switchWeather: function() {
			if (weatherLayer.getMap() === undefined || weatherLayer.getMap() === null) {
				this.enableWeather();
			} else {
				this.disableWeather();
			}
		},



		search: function(options) {
			if (com.servinfo.consumer === undefined && com.servinfo.consumer.Consumer === undefined) {
				alert('No sa ha incluido la libreria de consumo de webservices.');
				return false;
			}

			var radius = options.radius || 1000;
			var entity = options.entity || '';
			var term = options.term || '';
			var layer = options.layer || currentLayer;

			consumer.getEntity({
				entity: entity,
				latitude: latitude,
				longitude: longitude,
				radius: radius,
				term: term,
				layer: layer,
				callback: function(list, layer) {
					for (var i in list) {
						var item = list[i];
						viewer.addMarker({
							id: item.id,
							latitude: item.latitude,
							longitude: item.longitude,
							name: item.title,
							icon: item.icon,
							layer: layer,
							info: item.info,
							onClick: function(id, event) {
								var marker = viewer.getMapOverlay(id);
								var lat = marker.geometry.getPosition().lat();
								var lng = marker.geometry.getPosition().lng();
								var contentString = '<div class="info-window-detail"><table>';
								for (var j in marker.info) {
									if (marker.info[j] !== undefined && marker.info[j] !== null && marker.info[j] !== '') {
										contentString += '<tr><th>' + j + '</th><td>' + marker.info[j] + '</td></tr>';
									}
								}
								contentString += '<tr><th colspan="2"><button class="btn btn-primary" onclick="walkRouting(' + lat + ', ' + lng + ');">Ruta A Pie</button>';
								contentString += '<button class="btn btn-primary" onclick="carRouting(' + lat + ', ' + lng + ');">Ruta En Auto</button>';
								contentString += '<button class="btn btn-primary" onclick="streetView(' + lat + ', ' + lng + ');">Street View</button></th></tr>';
								contentString += '</table>';
								contentString += '</div>';
								viewer.showInfowindowForOverlay(id, contentString, event.latLng);
							}
						});
					}
				}
			});
		}
	});

	this.init();



	function createMap() {
		$('#' + mapContainerId).append('<div id="mapDiv" style="width: 100%; height: 100%;" />');
		$('#mapDiv').css('position', 'relative');
		$('#mapDiv').css('float', 'left');
		//$('#mapDiv').css('z-index', '0');
		if (viewerType === com.servinfo.geoviewer.ViewerType.MAP) {
			$('#mapDiv').css('visibility', 'visible');
		} else {
			$('#mapDiv').css('visibility', 'hidden');
		}
		var mapOptions = {
			center: new google.maps.LatLng(latitude, longitude),
			zoom: options.zoom || zoom,
			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.LEFT_TOP
			},
			mapTypeControl: true,
			mapTypeId: mapType,
			mapTypeControlOptions: {
				position: google.maps.ControlPosition.LEFT_BOTTOM,
				mapTypeIds: ['OSM', google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.TERRAIN]
			},
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				style: google.maps.ZoomControlStyle.SMALL
			},
			streetViewControl: true,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			}
		};
		mapObject = new google.maps.Map($('#mapDiv')[0], mapOptions);

		drawingManager = new google.maps.drawing.DrawingManager({
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM,
				drawingModes: [
					google.maps.drawing.OverlayType.MARKER,
					google.maps.drawing.OverlayType.POLYLINE,
					google.maps.drawing.OverlayType.POLYGON,
					google.maps.drawing.OverlayType.RECTANGLE
				]
			},
			circleOptions: {
				editable: true,
				strokeColor: "#0056B9",
				fillColor: '#ffffff',
				fillOpacity: 0.5,
			},
			rectangleOptions: {
				editable: false,
				strokeColor: "#0056B9",
				fillColor: '#ffffff',
				fillOpacity: 0.5,
			},
			polygonOptions: {
				editable: true,
				strokeColor: "#0056B9",
				fillColor: '#ffffff',
				fillOpacity: 0.5,
			},
			polylineOptions: {
				editable: true,
				strokeColor: "#0056B9",
				fillColor: '#ffffff',
				fillOpacity: 0.5,
			}
		});

		if (init_drawManager) {
			drawingManager.setMap(mapObject);
		};

		google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
			if (dispatchCustomEvent) {
				dispatchCustomEvent("DRAW_OVERLAY_COMPLETE", {
					detail: {
						event_overlay: event
					}
				});
			}
			//event.overlay.setMap(null);
			//mapObject.fitBounds(event.overlay.getBounds());
		});

		clusterer = new MarkerClusterer(mapObject);

		userMarker = new google.maps.Marker({
			position: new google.maps.LatLng(latitude, longitude),
			title: 'Mi Posición',
			icon: imgPath + 'user_marker.gif',
			optimized: false,
			map: null
		});


		if (init_userMarker == true) {
			userMarker.setMap(mapObject);
		} else {
			userMarker.setMap(null);
		}

		google.maps.event.addListener(userMarker, 'click', function(event) {
			var contentString = '<div class="info-window-detail"><table>';
			contentString += '<tr><th>Mi Posición</th></tr>';
			contentString += '</table>';
			contentString += '</div>';
			infowindow.setContent(contentString);
			infowindow.setPosition(event.latLng);
			infowindow.open(mapObject, this);
		});

		if (navigator.geolocation) {
			//El navegador soporta localización. Obtener ubicación
			if (dorequestPosition) {
				navigator.geolocation.getCurrentPosition(successPosition, failurePosition);
			}
		}

		google.maps.event.addListener(mapObject, 'center_changed', function(event) {
			latitude = this.getCenter().lat();
			longitude = this.getCenter().lng();
			dispatchCustomEvent("ONCENTERCHANGE_MAP", {
				detail: {
					//event_map : event 
				}
			});
		});

		google.maps.event.addListener(mapObject, 'zoom_changed', function(event) {
			if (viewerType === com.servinfo.geoviewer.ViewerType.MAP) {
				zoom = this.getZoom();
				if (onZoomChangeMap) {
					onZoomChangeMap(zoom);
				}

				dispatchCustomEvent("ONZOOMCHANGED_MAP", {
					detail: {
						event_map: {
							zoom: zoom
						}
					}
				});
			}
		});

		google.maps.event.addListener(mapObject, 'click', function(event) {
			infowindow.close();
			dispatchCustomEvent("ONCLICK_MAP", {
				detail: {
					event_map: event
				}
			});
		});

		google.maps.event.addListener(mapObject, 'rightclick', function(event) {
			dispatchCustomEvent("ONRIGHTCLICK_MAP", {
				detail: {
					event_map: event
				}
			});
		});

		google.maps.event.addListener(mapObject, 'idle', function(event) {
			dispatchCustomEvent("ONIDLE_MAP", {
				detail: {
					event_map: event
				}
			});
		});

		geocoder = new google.maps.Geocoder();
		directionsService = new google.maps.DirectionsService();
		directionsRenderer = new google.maps.DirectionsRenderer();
		directionsRenderer.setMap(mapObject);
		updateLocation();

		trafficLayer = new google.maps.TrafficLayer();
		weatherLayer = new google.maps.weather.WeatherLayer({
			temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS
		});

		panoramio = new google.maps.panoramio.PanoramioLayer();

		if (onCompleteCreateMap) {
			onCompleteCreateMap();
		}

		dispatchCustomEvent("ONCOMPLETE_CREATEMAP", {
			detail: {}
		});

	}

	function successPosition(location, dozoom) {
		latitude = location.coords.latitude;
		longitude = location.coords.longitude;
		mapObject.setCenter(new google.maps.LatLng(latitude, longitude));
		if (dozoom) {
			mapObject.setZoom(15);
		};
		userMarker.setPosition(new google.maps.LatLng(latitude, longitude));
		updateLocation();
	}



	function failurePosition(error) {
		console.log(error);
	}



	//Actualizar la ciudad y país
	function updateLocation(callback, failure) {
		geocoder.geocode({
			latLng: mapObject.getCenter()
		}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				var result = results[0];
				for (var i in result.address_components) {
					var component = result.address_components[i];
					var types = component.types;
					if (types.indexOf('locality') !== -1 && types.indexOf('political') !== -1) {
						currentCity = component.long_name;
					}
					if (types.indexOf('country') !== -1 && types.indexOf('political') !== -1) {
						currentCountry = component.long_name;
					}
				}
				if (callback !== undefined) {
					callback();
				}
			} else {
				if (failure !== undefined) {
					failure();
				}
				return false;
			}
		});
	}



	function addMarker(options) {
		infowindow.close();
		var id = options.id || '' + new Date().getTime();
		if (getMapOverlay(id) !== undefined) {
			showOverlay(id);
			return;
		}
		var layer = options.layer || currentLayer;
		var latitude = options.latitude || 0;
		var longitude = options.longitude || 0;
		var name = options.name || id;
		var info = options.info || {};
		var iconImg = options.icon || '';
		var panToMarker = options.panToMarker;
		var onClick = options.onClick;
		var onComplete = options.onComplete;
		var balloonCont = options.balloon || 'PlaceMark';

		checkLayer(layer);
		var marker;

		if (iconImg !== '') {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(latitude, longitude),
				title: name,
				icon: imgPath + iconImg,
				map: null
			});
			if (mapLayers[layer].visible) {
				marker.setMap(mapObject);
			}

		} else {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(latitude, longitude),
				title: name,
				map: null
			});
			if (mapLayers[layer].visible) {
				marker.setMap(mapObject);
			}
		}
		if (useCluster)
			clusterer.addMarker(marker);
		google.maps.event.addListener(marker, 'click', function(event) {
			if (onClick !== undefined) {
				onClick(id, event);
			}
		});
		if (onComplete !== undefined) {
			onComplete(marker);
		}
		var obj = {
			type: 'marker',
			id: id,
			name: name,
			info: info,
			visible: true,
			geometry: marker,
			marker: iconImg,
			info: options.info
		};
		mapLayers[layer].overlays.setItem(id, obj);
		if (panToMarker !== undefined) {
			mapObject.setCenter(new google.maps.LatLng(latitude, longitude));
		}

		if (earthObject !== undefined) {
			var placemark = earthObject.createPlacemark('');
			placemark.setName(name);
			if (iconImg !== undefined) {
				var icon = earthObject.createIcon('');
				var iconHref = "http://" + document.domain + "/" + appName + "/" + imgPath + iconImg;
				icon.setHref(iconHref);
				var style = earthObject.createStyle('');
				style.getIconStyle().setIcon(icon);
				placemark.setStyleSelector(style);
			}
			var point = earthObject.createPoint('');
			point.setLatitude(parseFloat(latitude));
			point.setLongitude(parseFloat(longitude));
			placemark.setGeometry(point);
			google.earth.addEventListener(placemark, 'click', function(event) {
				event.preventDefault();
				var balloon = earthObject.createHtmlStringBalloon('');
				balloon.setFeature(event.getTarget());
				balloon.setMaxWidth(400);
				balloon.setContentString(balloonCont);
				earthObject.setBalloon(balloon);
			});

			//placemark.setName(name);
			earthObject.getFeatures().appendChild(placemark);
			var obj = {
				type: 'marker',
				id: id,
				name: name,
				visible: true,
				geometry: placemark,
				marker: iconImg
			}
			earthLayers[layer].overlays.setItem(id, obj);
		}

		return id;
	}



	function initializeDraw() {
		marker = new google.maps.Marker({
			title: name,
			icon: 'img/icons-markers/default.png'
		});
		polyLine = new google.maps.Polyline({
			path: [],
			editable: true,
			strokeColor: "#FF00AA",
			strokeOpacity: 0.5,
			strokeWeight: 2
		});
		polyLine.setMap(mapObject);

		polygon = new google.maps.Polygon({
			paths: [],
			editable: true,
			strokeColor: "#FF00AA",
			strokeOpacity: 0.5,
			strokeWeight: 2,
			fillColor: "#FF00AA",
			fillOpacity: 0.1
		});
		polygon.setMap(mapObject);
	}



	function finishDraw() {
		if (drawMode === com.servinfo.geoviewer.DrawMode.MARKER) {
			if (drawing === true) {
				marker.setDraggable(false);
				var id = 'Marker_' + new Date().getTime();
				var obj = {
					type: 'marker',
					id: id,
					name: id,
					visible: true,
					geometry: marker,
					marker: 'img/icons-markers/default.png',
					address: '',
					saved: false
				};
				checkLayer(currentLayer);
				mapLayers[currentLayer].overlays.setItem(id, obj);
				dispatchCustomEvent("DRAW_FINISH", {
					detail: {
						object: obj
					}
				});
				initializeDraw();
			}
		}
		if (drawMode === com.servinfo.geoviewer.DrawMode.LINE) {
			if (drawing === true) {
				polyLine.setEditable(false);
				if (polyLine.getPath().getLength() > 1) {
					var id = 'Line_' + new Date().getTime();
					var obj = {
						type: 'line',
						id: id,
						name: id,
						visible: true,
						color: '#FF00AA',
						geometry: polyLine,
						saved: false
					};
					checkLayer(currentLayer);
					mapLayers[currentLayer].overlays.setItem(id, obj);
					dispatchCustomEvent("DRAW_FINISH", {
						detail: {
							object: obj
						}
					});
				} else {
					polyLine.getPath().clear();
				}
				initializeDraw();
			}
		}
		if (drawMode === com.servinfo.geoviewer.DrawMode.POLY) {
			if (drawing === true) {
				polygon.setEditable(false);
				if (polygon.getPath().getLength() > 2) {
					var id = 'Poly_' + new Date().getTime();
					var obj = {
						type: 'poly',
						id: id,
						name: id,
						visible: true,
						color: '#FF00AA',
						geometry: polygon,
						saved: false
					};
					checkLayer(currentLayer);
					mapLayers[currentLayer].overlays.setItem(id, obj);
					dispatchCustomEvent("DRAW_FINISH", {
						detail: {
							object: obj
						}
					});
				} else {
					polygon.getPath().clear();
				}
				initializeDraw();
			}
		}
		drawing = false;
	}



	function createGlobe() {
		if (google.earth.isSupported()) {
			$('#' + mapContainerId).append('<div id="globeDiv" style="width: 100%; height: 100%;" />');
			google.earth.createInstance('globeDiv', successCB, failureCB);
			$('#globeDiv').css('position', 'absolute');
			$('#globeDiv').css('float', 'left');
			if (viewerType === com.servinfo.geoviewer.ViewerType.EARTH) {
				$('#globeDiv').css('visibility', 'visible');
			} else {
				$('#globeDiv').css('visibility', 'hidden');
			}
		}
	}



	function successCB(instance) {
		//$('#globeDiv').css('width', '100%');
		// $('#globeDiv').css('height', '100%');
		//$('#globeDiv').hide();

		earthObject = instance;
		earthObject.getLayerRoot().enableLayerById(earthObject.LAYER_BORDERS, true); //
		earthObject.getLayerRoot().enableLayerById(earthObject.LAYER_BUILDINGS, true);
		//earthObject.getLayerRoot().enableLayerById(earthObject.LAYER_ROADS, true);
		earthObject.getNavigationControl().setVisibility(earthObject.VISIBILITY_SHOW);
		earthObject.getWindow().setVisibility(true);

		var lookAt = earthObject.getView().copyAsLookAt(earthObject.ALTITUDE_RELATIVE_TO_GROUND);
		lookAt.setLatitude(latitude);
		lookAt.setLongitude(longitude);
		lookAt.setRange(zoom);
		earthObject.getView().setAbstractView(lookAt);

		var placemark = earthObject.createPlacemark('');
		placemark.setName("Mi Posición");

		gex = new GEarthExtensions(earthObject);
		// Define a custom icon.
		var icon = earthObject.createIcon('');
		icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
		var style = earthObject.createStyle(''); //create a new style
		style.getIconStyle().setIcon(icon); //apply the icon to the style
		placemark.setStyleSelector(style); //apply the style to the placemark

		// Set the placemark's location.  
		var point = earthObject.createPoint('');
		point.setLatitude(latitude);
		point.setLongitude(longitude);
		placemark.setGeometry(point);

		earthObject.getFeatures().appendChild(placemark);

		google.earth.addEventListener(earthObject.getView(), 'viewchangeend', function() {
			if (earthViewChangedTimer) {
				clearTimeout(earthViewChangedTimer);
			}
			if (viewerType === com.servinfo.geoviewer.ViewerType.EARTH) {
				earthViewChangedTimer = setTimeout(function() {
					var lookAt = earthObject.getView().copyAsLookAt(earthObject.ALTITUDE_RELATIVE_TO_GROUND);
					latitude = lookAt.getLatitude();
					longitude = lookAt.getLongitude();
					zoom = lookAt.getRange();
				}, 100);
			}
		});

		if (onCompleteCreateGlobe) {
			onCompleteCreateGlobe();
		}
	}



	function failureCB(errorCode) {

		//alert('Error al cargar Google Earth. Código de error: ' + errorCode);
	}



	function zoomMapToEarth(zoom) {
		return Math.pow(10, 9) * Math.pow(2.718281, (-0.85 * parseFloat(zoom)));
	}



	function zoomEarthToMap(zoom) {
		return parseInt(Math.round(Math.log(zoom / Math.pow(10, 9)) / -0.85));
	}



	function renameLayer(id, newName) {
		mapLayers[id].name = newName;
		earthLayers[id].name = newName;
	}



	function checkLayer(layer) {
		if (layer === undefined || layer === '' || layer < 0) {
			layer = 0;
		}
		if (mapLayers[layer] === undefined) {
			mapLayers[layer] = new Object();
			mapLayers[layer].name = 'Capa_' + layer;
			mapLayers[layer].visible = true;
			mapLayers[layer].overlays = new com.servinfo.Hash();
		}
		if (earthLayers[layer] === undefined) {
			earthLayers[layer] = new Object();
			earthLayers[layer].name = 'Capa_' + layer;
			earthLayers[layer].visible = true;
			earthLayers[layer].overlays = new com.servinfo.Hash();
		}
	}



	function hideLayer(layer) {
		if (mapLayers[layer] !== undefined) {
			mapLayers[layer].overlays.each(function(overlay) {
				overlay.geometry.setMap(null);
				if (overlay.type === 'marker') {
					clusterer.removeMarker(overlay.geometry);
				}
			});
			clusterer.redraw();
			mapLayers[layer].visible = false;
		}
		if (earthLayers[layer] !== undefined) {
			earthLayers[layer].overlays.each(function(overlay) {
				earthObject.getFeatures().removeChild(overlay.geometry);
			});
			earthLayers[layer].visible = false;
		}
	}



	function showLayer(layer) {
		if (mapLayers[layer] !== undefined) {
			if (mapLayers[layer].visible === true) {
				return;
			}
			mapLayers[layer].overlays.each(function(overlay) {
				overlay.geometry.setMap(mapObject);
				if (useCluster)
					if (overlay.type === 'marker') {
						clusterer.addMarker(overlay.geometry);
					}
			});
			clusterer.redraw();
			mapLayers[layer].visible = true;
		}
		if (earthLayers[layer] !== undefined) {
			earthLayers[layer].overlays.each(function(overlay) {
				earthObject.getFeatures().appendChild(overlay.geometry);
			});
			earthLayers[layer].visible = true;
		}
	}



	function deleteLayer(layer) {
		if (mapLayers[layer] !== undefined) {
			mapLayers[layer].overlays.each(function(overlay) {
				overlay.geometry.setMap(null);
				if (overlay.type === 'marker') {
					clusterer.removeMarker(overlay.geometry);
				}
			});
			clusterer.redraw();
			delete mapLayers[layer];
		}
		if (earthLayers[layer] !== undefined) {
			earthLayers[layer].overlays.each(function(overlay) {
				earthObject.getFeatures().removeChild(overlay.geometry);
			});
			delete earthLayers[layer];
		}
	}



	function getMapOverlay(id) {
		var result = undefined;
		for (var layer in mapLayers) {
			mapLayers[layer].overlays.each(function(overlay) {
				if (overlay.id === id) {
					result = overlay;
				}
			});
		}
		return result;
	}



	function getEarthOverlay(id) {
		var result = undefined;
		if (earthLayers !== undefined) {
			for (var layer in earthLayers) {
				earthLayers[layer].overlays.each(function(overlay) {
					if (overlay.id === id) {
						result = overlay;
					}
				});
			}
		}
		return result;
	}



	function showOverlay(id) {
		if (centering === true) {
			batchIds.push(id);
			batchCount++;
			if (batchCount === batchTotal) {
				centerBatch();
			}
		}
		var overlay = getMapOverlay(id);
		if (overlay !== undefined) {
			overlay.geometry.setOptions({
				map: mapObject
			});
			if (useCluster)
				if (overlay.type === 'marker') {
					clusterer.addMarker(overlay.geometry);
					overlay.geometry.setMap(mapObject);
				}
		}
		clusterer.redraw();
		overlay = getEarthOverlay(id);
		if (overlay !== undefined) {
			earthObject.getFeatures().appendChild(overlay.geometry);
		}
	}



	function centerBatch() {
		var bounds = new google.maps.LatLngBounds();
		for (var i in batchIds) {
			var poly = getMapOverlay(batchIds[i]);
			if (poly !== undefined) {
				bounds.union(poly.geometry.getBounds());
			}
		}
		mapObject.fitBounds(bounds);
		mapObject.setCenter(bounds.getCenter());
		console.log(mapObject.getZoom());

		if (earthObject !== undefined) {
			var lookAt = earthObject.getView().copyAsLookAt(earthObject.ALTITUDE_RELATIVE_TO_GROUND);
			lookAt.setLatitude(latitude);
			lookAt.setLongitude(longitude);
			zoom = zoomMapToEarth(mapObject.getZoom());
			console.log(zoom);
			lookAt.setRange(zoom);
			earthObject.getView().setAbstractView(lookAt);
		}

		centering = false;
		if (batchCallback !== undefined) {
			batchCallback();
		}
	}


};